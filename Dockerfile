FROM ubuntu:bionic
RUN apt update && apt install -y tor obfs4proxy && rm -rf /var/lib/{apt,dpkg,cache,log}/
ADD torrc.config /etc/tor/torrc
ADD entrypoint.sh /bin
ENTRYPOINT entrypoint.sh
