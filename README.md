docker : 
```
docker run -it -p 9050:9050 ahmadly/tor
```

compose:
```
version: '3.6'

services:
  tor:
    image: ahmadly/tor
    ports:
      - "9050:9050"
```
